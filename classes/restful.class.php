<?php

class restful {
	private $api;
	private $method;
	private $data = [];
	private $headers = [];

	function __construct($method,$path,$data,$headers) {
		$this->setMethod($method);
		$this->parseData($data);
		$this->headers = $headers;
		$this->parsePath($path);
	}

	private function setMethod( $method ) {
		if (in_array($method,["GET","POST","PUT","DELETE","OPTIONS"])) {
			$this->method = $method;
		}
	}

	private function parsePath( $path ) {
		$p = explode("/",trim($path,"/"),3);
		if (count($p) > 1) {
			if (count($p) == 2) { $p[2] = ""; }
			$this->loadApi($p[0],$p[1],$p[2]);
		}
	}

	private function parseData( $data ) {
		$this->data = json_decode($data,true);
	}

	private function loadApi($api,$version,$endpoint) {
		$class = $api."_".$version;
		if (!class_exists($class)) {
			$path = PATH."/apis/${class}.php";
			if (file_exists($path)) {
				require_once $path;
			}
		}
		$this->api = new $class($this->method,$endpoint,$this->data,$this->headers);
	}

	public function getResult() {
		if (isset($this->api)) {
			$result = $this->api->executeEndpoint();
			$status = $this->api->getStatus();
			if (!empty($status)) {
				$result["status"] = $status;
			}
		} else {
			http_response_code(404);
			$result = array("status" => "api not found");
		}
		header("Access-Control-Allow-Origin: *");
		header("Content-type:application/json; charset=utf-8");
		return json_encode($result,JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
	}
}