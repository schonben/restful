<?php

abstract class api {
	private $endpointData;
	private $data;
	protected $status = array( 200, "" );

	// Force Extending class to define this method
	abstract protected function endPoints();

	// Not required to
	protected function parseHeaders( $headers ) {
	}

	public function __construct( $method, $endpoint, $data, $headers ) {
		// Parse the headers.
		$this->parseHeaders( $headers );
		// Get the endpoint data.
		$this->endpointData = $this->getEndpointData( $method, $endpoint );
		// Save the passed data.
		$this->data = $data;
	}

	protected function getHeaderValue( $headers, $key, $default="" ) {
		// Allow the client to send this header.
		header("Access-Control-Allow-Headers: ".$key);
		// Read the value
		if (empty($headers["DashId"])) {
			return $default;
		}
		return $headers["DashId"];
	}

	protected function getEndpointData( $method, $endpoint ) {
		if ( empty( $endpoint ) ) {
			// Show endpoint info
			$info = [];
			foreach ( $this->endPoints() as $endpoint ) {
				$info[] = $this->formatInfo($endpoint);
			}
			return array(
				"endpointInfo" => $info,
			);
		} elseif ( $method === "OPTIONS" ) {
			$allow = [];
			$info = [];
			foreach ( $this->endPoints() as $ep ) {
				if ($this->matchEndpoint($ep["path"],$endpoint) !== false) {
					$allow[] = strtoupper($ep["method"]);
					$info[] = $this->formatInfo($ep);
				}
			}
			return array(
				"allow" => $allow,
				"endpointInfo" => $info,
			);
		} else {
			foreach ( $this->endPoints() as $ep ) {
				if ( strtoupper( $ep["method"] ) == $method ) {
					$returnVars = $this->matchEndpoint( $ep["path"], $endpoint );
					if ($returnVars !== false) {
						return array(
							"handler" => $ep["handler"],
							"vars"    => $returnVars
						);
					}
				}
			}
		}
	}

	private function formatInfo($endpoint) {
		return array(
			"path"        => trim( $endpoint["path"], "/" ),
			"method"      => $endpoint["method"],
			"description" => empty( $endpoint["description"] ) ? "" : $endpoint["description"],
		);
	}

	private function matchEndpoint( $path, $endpoint ) {
		// Extract variables from path.
		$path = trim( $path, "/" );
		$returnVars = [];
		$pathVars   = [];
		if ($path == $endpoint) {
			return $returnVars;
		} elseif ( preg_match_all( "|:([a-z]+)|", $path, $vars, PREG_SET_ORDER ) ) {
			$searchPath = "|^" . $path . "$|";
			foreach ( $vars as $var ) {
				$pathVars[] = $var[1];
				$searchPath = str_replace( $var[0], "([^/]+)", $searchPath );
			}
			if ( preg_match( $searchPath, $endpoint, $values ) ) {
				foreach ( $pathVars as $key => $value ) {
					$returnVars[ $value ] = $values[ $key + 1 ];
				}
				return $returnVars;
			}
		}
		return false;
	}

	public function executeEndpoint() {
		if ( ! empty( $this->endpointData["allow"] ) ) {
			// Set the headers for OPTIONS data, info will be returned below.
			header("Allow: ".implode(",",$this->endpointData["allow"]));
		}
		if ( ! empty( $this->endpointData["endpointInfo"] ) ) {
			// Return the endpointInfo
			return $this->endpointData["endpointInfo"];
		} elseif ( ! empty( $this->endpointData["handler"] ) && method_exists( $this, $this->endpointData["handler"] ) ) {
			// Execute the endpoint function
			$vars = $this->endpointData["vars"];
			array_unshift( $vars, $this->data );
			$result = call_user_func_array( array( $this, $this->endpointData["handler"] ), $vars );

			return $result;
		} else {
			// Endpoint is not found
			$this->setError( "endpoint not found", 404 );
			return [];
		}
	}

	public function setError( $message, $code = 500 ) {
		$this->status = [ $code, $message ];
	}

	public function getStatus() {
		if ( $this->status[0] != 200 ) {
			http_response_code( $this->status[0] );
		}

		return $this->status[1];
	}
}