<?php

class dashboard_v1 extends api {
	private $dashId = "";

	protected function endPoints () {
		return array(
			array(
				"method" => "get",
				"path" => "/test/:test/yo/:foo/",
				"handler" => "get_test",
			),
			array(
				"method" => "get",
				"path" => "screen",
				"handler" => "get_screen",
			),
			array(
				"method" => "post",
				"path" => "screen",
				"handler" => "get_screen",
			),
		);
	}

	protected function parseHeaders($headers) {
		$this->dashId = $this->getHeaderValue($headers,"DashId");
	}

	public function get_screen ($data) {
		$result = array(
			"dashId" => $this->dashId,
			"hello" => "bar",
			"array" => [3,4,6,3,2,1],
		);

		return $result;
	}
}