<?php

require_once "../init.php";

$data = file_get_contents("php://input");
$headers = getallheaders();

if (isset($_SERVER['REDIRECT_URL'])) {
	$restful = new restful( $_SERVER['REQUEST_METHOD'], $_SERVER['REDIRECT_URL'],$data,$headers, $_SERVER['QUERY_STRING'] );

	echo $restful->getResult();
}
