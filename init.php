<?php

define("PATH",realpath(dirname(__FILE__)));

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

function restfulErrorHandler($errno, $errstr, $errfile, $errline) {
	// TODO This needs to be handled in the framework.
	echo "Error: [$errno] $errstr<";
	echo "Error on line $errline in $errfile";
}
//set_error_handler("restfulErrorHandler",E_ALL);

spl_autoload_register(function ($name) {
	if (ctype_alnum($name) && !class_exists($name)) {
		$path = PATH."/classes/${name}.class.php";
		if (file_exists($path)) {
			require_once $path;
			return true;
		}
		trigger_error("Class '$name' can't be loaded.");
	}
});

